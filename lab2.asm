section .data
    arr db 3, -5, 3, 0, 8, -1, 7, 4, -2, 7 ; исходный массив
    arr_len equ 10 ; длина массива
    A equ 2 ; индекс начала суммирования
    B equ 7 ; индекс конца суммирования
    error_msg db "Error: Invalid input", 0
    overflow_msg db "Error: Overflow occurred", 0
    sum_msg db "Sum bytes: %d", 10, 0
    max_byte_value equ 127 ; максимально возможное значение байта
    min_byte_value equ -128 ; минимально возможное значение байта

section .text
    global main
    extern printf

main:
    mov rbp, rsp
    push rbp
    
    mov rdx, A ; индекс начала суммирования
    mov rbx, B+1 ; индекс конца суммирования
    mov rcx, 0 ; сумма положительных байтов
    
    ; проверка входных данных
    cmp rdx, rbx ; проверяем, что начало суммирования меньше конца
    jg error
    
    cmp rdx, arr_len ; проверяем, что начало суммирования в пределах массива
    jge error
    
    cmp rbx, arr_len ; проверяем, что конец суммирования в пределах массива
    jge error

loop_start:
    mov al, [arr + rdx - 1] ; загружаем байт
    movsx rax, al ; преобразуем значение в 64-битное
    
    ; проверка на переполнение
    cmp rax, max_byte_value ; проверяем, что значение байта не больше 127
    jg overflow
    cmp rax, min_byte_value ; проверяем, что значение байта не меньше -128
    jl overflow
    
    cmp rax, 0 ; проверяем знак
    jle skip_byte
    add rcx, rax ; добавляем положительное значение к сумме
skip_byte:
    inc rdx ; увеличиваем индекс текущего байта
    cmp rdx, rbx ; проверяем, достигли ли мы конца суммирования
    jl loop_start
    
    mov rdi, sum_msg
    mov rsi, rcx
    mov rax, 0           
    call printf
    pop rbp
    
    mov rax, 0
    ret
    
error:
    mov rdi, error_msg
    mov rax, 0           
    call printf
    pop rbp
    mov rax, 1
    ret
    
overflow:
    mov rdi, overflow_msg
    mov rax, 0           
    call printf
    pop rbp
    mov rax, 1
    ret
