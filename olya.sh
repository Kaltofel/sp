#!/bin/bash
touch /root/koa.txt
while true; do
    echo "Автор: Коробейникова Ольга Андреевна"
    echo "Название программы: Файлы и разрешения"
    echo "Краткое описание: Программа для вывода прав введенного пользователя к введенному файлу в формате: ЧИТАТЬ/ПИСАТЬ/ИСПОЛНЯТЬ"
    echo "Текущий каталог: $(pwd)"
    while true; do
        read -p "Введите имя файла: " filename
        if [ -f "$filename" ]; then
            break
        else
            echo "Ошибка: файл не существует" >&2
        fi
    done
    read -p "Введите имя пользователя: " username
    if [ $(id -u "$username") -eq 0 ] || [ "$(ls -ld "$filename" | awk '{print $3}')" = "$username" ]; then
        echo -n "Права пользователя $username к файлу $filename: "
        [ -r "$filename" ] && echo -n "чтение "
        [ -w "$filename" ] && echo -n "запись "
        [ -x "$filename" ] && echo -n "исполнение"
        echo
    fi
    read -p "Хотите продолжить? (y/n) " answer
    if [ "$answer" != "y" ]; then
        exit 0
    fi
done
